#!/usr/bin/env python

import pygtk
pygtk.require('2.0')
import gtk
import pango
import gobject
import optparse
import sys

import env


class MainWindow(object):
    def __init__(self):
        self.__window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.__window.connect("destroy", self.close)
        self.__window.set_size_request(1024, 768)
        size = self.__window.get_allocation()

        self.__area = gtk.DrawingArea()
        self.__area.connect("expose-event", self.expose_event)
        self.__area.set_size_request(size.width, size.height)
        self.__window.add(self.__area)

        self.__error = None
        self.__last_results = [(), ()]

    def show(self, fullscreen=True):
        self.__window.show_all()

        if fullscreen:
            self.__window.fullscreen()

    def hide(self):
        self.__window.hide()

    def close(self, widget, data=None):
        gtk.main_quit()

    def expose_event(self, area, event):
        if not self.__error:
            self.__draw_results()

        else:
            red_gc = self.__area.window.new_gc(foreground=self.__area.window.get_colormap().alloc_color(gtk.gdk.Color(0xAAAA, 0, 0)))
            self.__draw_bordered(self.__area, red_gc, self.__area.get_allocation(), "Error: %s" % str(self.__error))

    def handle_error(self, error):
        self.__error = error
        self.__area.queue_draw()

    def handle_results(self, passed, failed):
        self.__error = None
        self.__last_results = [passed, failed]
        self.__area.queue_draw()

    def __draw_results(self, limit=4):
        size = self.__area.get_allocation()
        gdk_window = self.__area.window
        bg = gdk_window.get_colormap().alloc_color(gtk.gdk.Color(0, 0, 0))
        red_gc = gdk_window.new_gc(foreground=gdk_window.get_colormap().alloc_color(gtk.gdk.Color(0xAAAA, 0, 0)), background=bg)
        green_gc = gdk_window.new_gc(foreground=gdk_window.get_colormap().alloc_color(gtk.gdk.Color(0, 0xAAAA, 0)), background=bg)
        current = gtk.gdk.Rectangle(0, 0, size.width, size.height)

        passed, failed = self.__last_results

        failed.sort()
        complete = failed + passed

        dir = 0
        for i, test in enumerate(complete[:-1]):
            if i > limit:
                break

            gc = green_gc
            if i < len(failed):
                gc = red_gc

            if dir == 0: # Vertical split
                draw_area = gtk.gdk.Rectangle(current.x, current.y, current.width / 2, current.height)
                current = gtk.gdk.Rectangle(current.x + current.width / 2, current.y, current.width / 2, current.height)

            else: # Horizontal split
                draw_area = gtk.gdk.Rectangle(current.x, current.y, current.width, current.height / 2)
                current = gtk.gdk.Rectangle(current.x, current.y + current.height / 2, current.width, current.height / 2)

            self.__draw_bordered(self.__area, gc, draw_area, text=(test.replace('.', '. ') if i < limit else None))
            dir = 1 - dir

        self.__draw_bordered(self.__area, (red_gc if i <= len(failed) else green_gc), current)

    def __draw_bordered(self, window, gc, area, text=None, border_width=2):
        gdk_window = window.window
        if border_width:
            fg = gc.foreground
            gc.foreground = gc.background
            gdk_window.draw_rectangle(gc, True, area.x, area.y, area.width, area.height)
            gc.foreground = fg

        gdk_window.draw_rectangle(gc, True, area.x + border_width, area.y + border_width, area.width - border_width, area.height - border_width)

        if text is not None:
            text = str(text)

            font = pango.FontDescription('Courier 14')
            font.set_weight(pango.WEIGHT_BOLD)

            layout = window.create_pango_layout(text)
            layout.set_font_description(font)
            layout.set_width(area.width * pango.SCALE)

            x = area.width / 2 + area.x
            y = area.height / 2 + area.y

            lay_w, lay_h = layout.get_pixel_size()
            x -= lay_w / 2
            y -= lay_h / 2

            text_gc = gdk_window.new_gc()

            gdk_window.draw_layout(text_gc, x, y, layout)


if __name__ == '__main__':
    parser = optparse.OptionParser(usage='%prog [ options ]')
    parser.add_option('-r', '--repo', default=None,
                      help='Set git repository')
    parser.add_option('-t', '--test-dir', default='tests',
                      help='Test directory relative to repository')
    parser.add_option('-F', '--no-fullscreen', default=True, action='store_false',
                      help='Do not run in full screen mode')
    parser.add_option('-T', '--timeout', default=10, type=int,
                      help='Update interval of repository (in sec)')

    opts, args = parser.parse_args()

    if opts.repo is None:
        print >> sys.stderr, "No repository given"
        exit(1)

    window = MainWindow()
    window.show(opts.no_fullscreen)
    runner = env.GitRunner(opts.repo, opts.test_dir)
    env.BackgroundRunner(runner, window, opts.timeout * 1000)
    gtk.main()
