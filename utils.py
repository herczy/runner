import subprocess
import shlex


def run_command_raw(cmd, **kwargs):
    print 'running %s' % cmd
    args = shlex.split(cmd)
    p = subprocess.Popen(args, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               **kwargs)
    stdout, stderr = p.communicate()
    print '  rc=', p.returncode

    return p.returncode, stdout, stderr


def run_command(cmd, expect=0, **kwargs):
    code, out, err = run_command_raw(cmd, **kwargs)
    if code != expect:
        raise RuntimeError("Command '%s' exited with code %d" % (cmd, code))
