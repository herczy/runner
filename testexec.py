import tempfile
from lxml import etree
import os
import sys

from utils import run_command_raw


class ExecBase(object):
    def execute(self):
        raise NotImplementedError("ExecBase.execute")


class NoseTest(ExecBase):
    def __init__(self, directory, cwd=None):
        self.__directory = directory
        self.__cwd = cwd

    def execute(self):
        with tempfile.NamedTemporaryFile() as ntemp:
            cmd = "/usr/bin/nosetests3 -w '%s' --with-xunit --xunit-file '%s'" % (self.__directory, ntemp.name)
            rc, out, err = self.__run_command(cmd)

            print '-----'
            print '----- command: %s' % cmd
            print '-----'
            print 'stdout:'
            print out
            print 'stderr:'
            print err
            print 'retcode:', rc
            print '-----'

            ntemp.seek(0)
            try:
                xml_root = etree.parse(ntemp)

            except etree.XMLSyntaxError:
                # Failed to run test, throw our own error
                raise RuntimeError("Failed to run tests")

        return self.__process(xml_root)

    def __run_command(self, cmd):
        if self.__cwd is None:
            return run_command_raw(cmd)

        cwd = os.getcwd()
        os.chdir(self.__cwd)
        try:
            return run_command_raw(cmd, env=dict(PYTHONPATH='.'))

        finally:
            os.chdir(cwd)

    def __process(self, xml_root):
        passed = []
        failed = []
        for case in xml_root.iterfind('testcase'):
            ident = '%s.%s' % (case.attrib['classname'], case.attrib['name'])
            if len(case) != 0:
                failed.append(ident)

            else:
                passed.append(ident)

        return passed, failed
