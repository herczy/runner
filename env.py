import os
import tempfile
import shutil

from utils import run_command
from testexec import NoseTest

import gobject


class GitRunner(object):
    def __init__(self, clone_url,
                       test_relpath,
                       test_class=NoseTest):
        self.__clone_url = clone_url
        self.__test_relpath = test_relpath
        self.__test_class = test_class
        self.__directory = None

    def run(self):
        self.__checkout()
        self.__update()

        tester = self.__test_class(self.__test_relpath, cwd=self.__directory)
        passed, failed = tester.execute()

        for f in failed:
            print 'FAILED: %s' % f

        return passed, failed

    def __del__(self):
        self.__cleanup()

    def __checkout(self):
        if self.__directory is None:
            self.__directory = tempfile.mkdtemp(prefix='testrunner-repo-')
            run_command("git clone '%s' ." % self.__clone_url, cwd=self.__directory)

    def __cleanup(self):
        if self.__directory is not None:
            shutil.rmtree(self.__directory)

    def __update(self):
        run_command('git fetch', cwd=self.__directory)
        run_command('git reset --hard origin/master', cwd=self.__directory)


class BackgroundRunner(object):
    def __init__(self, runner, handler, timeout):
        self.__runner = runner
        self.__handler = handler

        self.__source_id = gobject.timeout_add(timeout, self.__tick)
        self.__tick()

    def close(self):
        gobject.source_remove(self.__source_id)

    def __tick(self):
        try:
            passed, failed = self.__runner.run()
            self.__handler.handle_results(passed, failed)

        except RuntimeError, e:
            self.__handler.handle_error(e)

        return True
